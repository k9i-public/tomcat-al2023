FROM amazonlinux:2023

# https://docs.aws.amazon.com/corretto/latest/corretto-21-ug/amazon-linux-install.html
RUN set -eux; \
	dnf update; \
	dnf -y install gzip tar java-21-amazon-corretto-headless

RUN set -eux; \
    curl https://archive.apache.org/dist/tomcat/tomcat-10/v10.1.18/bin/apache-tomcat-10.1.18.tar.gz | tar pzxvf - -C /usr/local/; \
    mv -v /usr/local/apache-tomcat-10.1.18 /usr/local/tomcat

# Remove default webapp (manager)
RUN set -eux; \
	rm -rv /usr/local/tomcat/webapps/*

ENV PATH /usr/local/tomcat/bin:$PATH
RUN mkdir -pv /usr/local/tomcat
WORKDIR $CATALINA_HOME

# Overwrite config files, etc.
COPY files /

# smoke test
RUN	catalina.sh version

EXPOSE 8080
CMD ["catalina.sh", "run"]
