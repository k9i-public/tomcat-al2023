# am2023/java-21-amazon-corretto + apache.org tomcat
https://archive.apache.org/dist/tomcat/tomcat-10/v10.1.18/bin/apache-tomcat-10.1.18.tar.gz

- Server version: Apache Tomcat/10.1.18
- JVM Version:    21.0.2+13-LTS
- JVM Vendor:     Amazon.com Inc.

```
$ docker-compose --progress plain --ansi never exec app catalina.sh version
Using CATALINA_BASE:   /usr/local/tomcat
Using CATALINA_HOME:   /usr/local/tomcat
Using CATALINA_TMPDIR: /usr/local/tomcat/temp
Using JRE_HOME:        /usr
Using CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar
Using CATALINA_OPTS:   
Server version: Apache Tomcat/10.1.18
Server built:   Jan 5 2024 14:39:40 UTC
Server number:  10.1.18.0
OS Name:        Linux
OS Version:     6.7.3-arch1-1
Architecture:   amd64
JVM Version:    21.0.2+13-LTS
JVM Vendor:     Amazon.com Inc.
```

------------------------------------------------------------------------------
# am2023 標準tomcat (dnf -y install tomcat9)
- Server version name:   Apache Tomcat/9.0.82
- JVM Version:           17.0.10+7-LTS

```
bash-5.2# /usr/libexec/tomcat9/server start
Java virtual machine used: /usr/lib/jvm/jre/bin/java
classpath used: /usr/share/tomcat9/bin/bootstrap.jar:/usr/share/tomcat9/bin/tomcat-juli.jar:
main class used: org.apache.catalina.startup.Bootstrap
flags used: -Djavax.sql.DataSource.Factory=org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory
options used: -Dcatalina.base=/usr/share/tomcat9 -Dcatalina.home=/usr/share/tomcat9 -Djava.endorsed.dirs= -Djava.io.tmpdir=/var/cache/tomcat9/temp -Djava.util.logging.config.file=/usr/share/tomcat9/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
arguments used: start
NOTE: Picked up JDK_JAVA_OPTIONS:  --add-opens=java.base/java.lang=ALL-UNNAMED --add-opens=java.base/java.io=ALL-UNNAMED --add-opens=java.base/java.util=ALL-UNNAMED --add-opens=java.base/java.util.concurrent=ALL-UNNAMED --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
08-Feb-2024 14:14:31.390 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/9.0.82
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Dec 13 2023 00:00:00 UTC
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 9.0.82.0
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            6.7.3-arch1-1
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/lib/jvm/java-17-amazon-corretto.x86_64
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           17.0.10+7-LTS
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Amazon.com Inc.
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/share/tomcat9
08-Feb-2024 14:14:31.392 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/share/tomcat9
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.lang=ALL-UNNAMED
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.io=ALL-UNNAMED
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.util=ALL-UNNAMED
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.base/java.util.concurrent=ALL-UNNAMED
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djavax.sql.DataSource.Factory=org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory
08-Feb-2024 14:14:31.398 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.base=/usr/share/tomcat9
08-Feb-2024 14:14:31.399 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.home=/usr/share/tomcat9
08-Feb-2024 14:14:31.399 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.endorsed.dirs=
08-Feb-2024 14:14:31.399 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.io.tmpdir=/var/cache/tomcat9/temp
08-Feb-2024 14:14:31.399 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.config.file=/usr/share/tomcat9/conf/logging.properties
08-Feb-2024 14:14:31.399 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
08-Feb-2024 14:14:31.400 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent The Apache Tomcat Native library which allows using OpenSSL was not found on the java.library.path: [/usr/java/packages/lib:/usr/lib64:/lib64:/lib:/usr/lib]
08-Feb-2024 14:14:31.549 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
08-Feb-2024 14:14:31.567 INFO [main] org.apache.catalina.startup.Catalina.load Server initialization in [286] milliseconds
08-Feb-2024 14:14:31.588 INFO [main] org.apache.catalina.core.StandardService.startInternal Starting service [Catalina]
08-Feb-2024 14:14:31.588 INFO [main] org.apache.catalina.core.StandardEngine.startInternal Starting Servlet engine: [Apache Tomcat/9.0.82]
08-Feb-2024 14:14:31.593 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
08-Feb-2024 14:14:31.600 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in [33] milliseconds
```
